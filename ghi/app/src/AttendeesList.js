function AttendeesList(props) {
    return (
        <div className="container">
            <table className="table table-striped table-hover ">
                <thead>
                    <tr>
                        <th>Name</th>
                        <th>Conference</th>
                    </tr>
                </thead>
                <tbody>
                    {props.attendees.map(attendees => {
                    return (
                        <tr key={attendees.href}>
                            <td>{attendees.name}</td>
                            <td>{attendees.conference}</td>
                        </tr>
                    );
                    })}
                </tbody>
            </table>
        </div>
    );
};
export default AttendeesList;
