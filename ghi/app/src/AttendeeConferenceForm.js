import React, { useEffect, useState } from 'react';

function AttendConferenceForm(props) {
    const [name, setName] = useState("");
    const [email, setEmail] = useState("");
    const [conference, setConference] = useState("");
    const [conferences, setConferences] = useState([]);

    const handleNameChange = (e) => {
        const value = e.target.value;
        setName(value);
    };
    const handleEmailChange = (e) => {
        const value = e.target.value;
        setEmail(value);
    };
    const handleConferenceChange = (e) => {
        const value = e.target.value;
        setConference(value)
    };
    const handleSubmit = async (e) => {
        e.preventDefault();

        const data = {};
        data.name = name;
        data.email = email;
        data.conference = conference;

        const attendeeUrl = 'http://localhost:8001/api/attendees/'
        const fetchConfig = {
            method: "post",
            body: JSON.stringify(data),
            headers: {
                'Content-Type': 'application/json',
            },
        };
        const response = await fetch(attendeeUrl, fetchConfig);
        if (response.ok) {
            const newAttendee = await response.json();

            setName("");
            setEmail("");
            setConference("");
        };
    };

    const fetchData = async () => {
        const url = "http://localhost:8000/api/conferences/";
        const response = await fetch(url);

        if (response.ok) {
            const data = await response.json();
            setConferences(data.conferences);
        };
    };

    useEffect(() => {
        fetchData();
    }, []);

    return (
        <>
        <div className='form-row'>
        <div className='offset-3 col-6'>
                <div className='shadow p-4 mt-4'>
                <h1>It's Conference Time!</h1>
                <form onSubmit={handleSubmit} id="create-attendee-form">
                <p>Please choose which conference you'd like to attend.</p>
                <div className='mb-3'>
                <select value={conference} onChange={handleConferenceChange} required name="conference" id="conference" className="form-select">
                        <option>Choose a conference</option>
                        {conferences.map(conference => {
                            return (
                                <option key={conference.name} value={conference.href}>
                                    {conference.name}
                                </option>
                            );
                        })};
                </select>
                </div>
                <p>Now, tell us about yourself.</p>
                    <div className='col form-floating mb-3'>
                        <input value={name} onChange={handleNameChange} placeholder='Name' required type='text' name='name' id='name' className='form-control'/>
                        <label htmlFor='name'>Your full name</label>
                    </div>
                    <div className="col form-floating mb-3">
                        <input value={email} onChange={handleEmailChange} placeholder="Email" required type="email" name="email" id="email" className="form-control"/>
                        <label htmlFor="email">Your email address</label>
                    </div>
                    <button className='btn btn-primary'>I'm going!</button>
                    </form>
                    </div>
        </div>
        </div>
        </>
    );
};
export default AttendConferenceForm;
