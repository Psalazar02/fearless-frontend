import React, { useEffect, useState } from 'react';

function ConferenceForm(props) {
    const [name, setName] = useState("");
    const [starts, setStart] = useState([]);
    const [ends, setEnd] = useState([]);
    const [description, setDescription] = useState("");
    const [maxPresentations, setMaxPresentation] = useState("");
    const [maxAttendees, setMaxAttendees] = useState("");
    const [locations, setLocations] = useState([]);
    const [location, setLocation] = useState("");

    const handleNameChange = (e) => {
        const value = e.target.value;
        setName(value);
    };
    const handleStartChange = (e) => {
        const value = e.target.value;
        setStart(value);
    };
    const handleEndChange = (e) => {
        const value = e.target.value;
        setEnd(value);
    };
    const handleDescriptionChange = (e) => {
        const value = e.target.value;
        setDescription(value);
    };
    const handleMaxPresentationChange = (e) => {
        const value = e.target.value;
        setMaxPresentation(value);
    };
    const handleMaxAttendeesChange = (e) => {
        const value = e.target.value;
        setMaxAttendees(value);
    };
    const handleLocationChange = (e) => {
        const value = e.target.value;
        setLocation(value);
    };
    const handleSubmit = async (e) => {
        e.preventDefault();

        const data = {};
        data.name = name;
        data.starts = starts;
        data.ends = ends;
        data.description = description;
        data.max_presentations = maxPresentations;
        data.max_attendees = maxAttendees;
        data.location = location;

        const conferenceUrl = 'http://localhost:8000/api/conferences/';
        const fetchConfig = {
            method: "post",
            body: JSON.stringify(data),
            headers: {
                'Content-Type': 'application/json',
            },
        };

        const response = await fetch(conferenceUrl, fetchConfig);
        if (response.ok) {
            const newConference = await response.json();

            setName('');
            setStart('');
            setEnd('');
            setDescription('');
            setMaxPresentation('');
            setMaxAttendees('');
            setLocation('');
        };
    };

    const fetchData = async () => {
        const url = 'http://localhost:8000/api/locations/';
        const response = await fetch(url);

        if (response.ok) {
            const data = await response.json();
            setLocations(data.locations);
        };
    };

    useEffect(() => {
        fetchData();
    }, []);

    return (
        <>
        <div className='row'>
        <div className='offset-3 col-6'>
                <div className='shadow p-4 mt-4'>
                <h1>Create a new conference</h1>
                <form onSubmit={handleSubmit} id="create-conference-form">
                <div className='form-floating mb-3'>
                    <input value={name} onChange={handleNameChange} placeholder='Name' required type='text' name='name' id='name' className='form-control'/>
                    <label htmlFor='name'>Name</label>
                </div>
                <div className="form-floating mb-3">
                    <input value={starts} onChange={handleStartChange} placeholder="Starts" required type="date" name="starts" id="starts" className="form-control"/>
                    <label htmlFor="starts">Starts</label>
                </div>
                <div className="form-floating mb-3">
                    <input value={ends} onChange={handleEndChange} placeholder="Ends" required type="date" name="ends" id="ends" className="form-control"/>
                    <label htmlFor="ends">Ends</label>
                </div>
                <div className="form-floating mb-3">
                    <input value={description} onChange={handleDescriptionChange} placeholder="Description" required type="text" name="description" id="description" className="form-control"/>
                    <label htmlFor="description">Description</label>
                </div>
                <div className="form-floating mb-3">
                    <input value={maxPresentations} onChange={handleMaxPresentationChange} placeholder="Max presentations" required type="number" name="max_presentations" id="max_presentations" className="form-control"/>
                    <label htmlFor="maxPresentations">Max presentations</label>
                </div>
                <div className="form-floating mb-3">
                    <input value={maxAttendees} onChange={handleMaxAttendeesChange} placeholder="Max attendees" required type="number" name="max_attendees" id="max_attendees" className="form-control"/>
                    <label htmlFor="maxAttendees">Max attendees</label>
                </div>
                <div className='mb-3'>
                <select value={location} onChange={handleLocationChange} required name="location" id="location" className="form-select">
                        <option>Choose a location</option>
                        {locations.map(location => {
                            return (
                                <option key={location.name} value={location.id}>
                                    {location.name}
                                </option>
                            );
                        })};
                </select>
                </div>
                <button className='btn btn-primary'>Create</button>
                </form>
                </div>
        </div>
        </div>
        </>
    )


}
export default ConferenceForm;
